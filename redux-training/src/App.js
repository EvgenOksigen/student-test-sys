import React from 'react';
import Admin from './Admin/index';
import redux from 'react-redux';

import './App.css';

function App() {
  return (
    <div className="App">
      <Admin></Admin>
    </div>
  );
}

export default App;
